(TeX-add-style-hook
 "articlepreamble"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("fontspec" "no-math") ("tcolorbox" "breakable") ("caption" "font=small" "labelfont=bf" "justification=justified" "format=plain") ("subcaption" "font=small" "labelfont=bf" "justification=justified" "format=plain") ("mtpro2" "subscriptcorrection" "nofontinfo" "zswash" "mtphrd" "mtpfrak" "mtpcal") ("scrlayer-scrpage" "headsepline") ("siunitx" "separate-uncertainty") ("geometry" "left=1cm" "right=1cm" "top=2cm" "bottom=1cm" "headsep=1cm") ("biblatex" "style=phys" "biblabel=brackets" "") ("hyperref" "hypertexnames=false" "hidelinks")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "href")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "etoolbox"
    "ifluatex"
    "tikz"
    "xpatch"
    "environ"
    "fontspec"
    "polyglossia"
    "import"
    "xifthen"
    "pdfpages"
    "tcolorbox"
    "bbding"
    "stfloats"
    "caption"
    "subcaption"
    "multirow"
    "tabularx"
    "amsmath"
    "xltxtra"
    "mtpro2"
    "graphicx"
    "scrlayer-scrpage"
    "csquotes"
    "siunitx"
    "geometry"
    "afterpage"
    "xcolor"
    "biblatex"
    "hyperxmp"
    "hyperref")
   (TeX-add-symbols
    '("parens" 1)
    '("globalcolor" 1)
    "oldsubfigure")
   (LaTeX-add-environments
    "partabstract")
   (LaTeX-add-polyglossia-langs
    '("english" "mainlanguage" "variant=british"))
   (LaTeX-add-xcolor-definecolors
    "fontcolour"
    "shadecolor"))
 :latex)

